<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no"/>
  <meta name="format-detection" content="telephone=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>One Page One Scroll | OPOS </title>
  <link rel="stylesheet" href="assets/stylesheets/style.css?ver=1.0">
</head>
<body>

  <?php
  /*------------------------------------------------------------------------------
  | OPOS Markup
  - Enmarcar las secciones en un div padre al cual se le aplicará el plugin
  - en este caso el padre es '.main'
  - Luego hijo directo estará dentro del sistema opos
  - Estos hijos sólo tienen que tener como obligarorio un id
  - En caso de que se desee un menú generado por el plugin opos agregar el atributo
  - data-name="", este atributo será el que aparecerá como bottón de sección
  --------------------------------------------------------------------------------
  <section id="page-01" data-name="Home">
    <!-- Contenido Aquí -->
  </section>
  ------------------------------------------------------------------------------*/
  ?>

  <div class="main" id="main">

      <section class="page page-transition page-01" id="page-01" data-name="Home">
        <div class="content-page">
          <h1>Sección 1</h1>
        </div>
      </section><!-- .page -->
      <section class="page page-transition page-02" id="page-02" data-name="Pedro">
        <div class="content-page">
          <h1>Sección 2</h1>
        </div>
      </section><!-- .page -->
      <section class="page page-transition page-03" id="page-03" data-name="Luis">
        <div class="content-page">
          <h1>Sección 3</h1>
        </div>
      </section><!-- .page -->
      <section class="page page-transition page-04" id="page-04" data-name="Damián Ramón">
        <div class="content-page">
          <h1>Sección 4</h1>
        </div>
      </section><!-- .page -->

  </div><!-- .main -->

  <script src="assets/javascripts/jquery-1.11.1.min.js"></script>
  <script src="assets/javascripts/opos.js"></script>
  <script type="text/javascript">

    $('.main').opos({
      // pageClass       : 'opos-page',
      // animationDelay  : 900,
      // transitionSpeed : 600,
      // transitionEase  :	"linear",
      // scrollSpeed     : {
      //   'chrome' : 14,
      //   'firefox': 16
      // },
      // navigationDots	: true,
      // navigationMenu	: false,
      // navContMenu		: '',
      // navActiveClass	: '.opos-scroll',
      // navActiveScroll: 30,
      // CustomURL				: {
      //   'activeURL'	: false,
      //   'basepath' 	: '',
      //   'segmentURL':	''
      // }
      // device         : 'desktop',
      // DisableOposWheel: '.opos-disable',
      // LockView				: '.fancybox-lock',
      // onFinishScroll : function(){}
      // beforeLoad			: function() {},
      // afterLoad			: function() {}
    });

  </script>
  </body>
</html>
