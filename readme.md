## Sinopsis

**OPOS**

Plugin creado para páginas one page con limitiación de uso de scroll.

Cada sección estará al alto de la pantalla como valor de alto mínimo.

Este plugin permite secciones más altas que la pantalla.

## Instalación

**Librerías necesarias**

**OPOS HTML**

- Enmarcar las secciones en un div padre al cual se le aplicará el plugin
- en este caso el padre es '.main'
- Luego el hijo directo estará dentro del sistema opos
- Estos hijos sólo tienen que tener como obligarorio un id
- En caso de que se desee un menú generado por el plugin opos agregar el atributo
- data-name="", este atributo será el que aparecerá como bottón de sección

```
  <div class="main">
    <section id="page-01" data-name="Home">
      <!-- Contenido Aquí -->
    </section>
  </div>

```



**OPOS CSS**

- Se creó un archivo en sass_partials llamado \_opos.scss que se deberá incluir en el archivo style.scss
- html,body por defecto tendrá 100% y height 100% con overflow: hidden
- En el mixin "opos" habrá que colocar el elemento padre de las secciones

```

@import 'sass_partials/opos';
@include opos('.main');

```


**OPOS JS**
- Requiere la librería jquery actualizada
- E incluir la librería opos.
<script src="assets/javascripts/opos.js"></script>
ó <script src="assets/javascripts/opos.min.js"></script>
- Luego llamar al elemento y aplicar el método opos.

```

  $('.main').opos({
    /* Clase que se asignará por defecto en las secciones */
    pageClass       : 'opos-page',

    /* Delay que esperará antes de hacer la animación de scroll */
    animationDelay  : 900,

    /* Velocidad de transición de sección a sección */
    transitionSpeed : 600,

    /* Ease, se puede añadir cualquier librería que contenga más tipos de ease
    y aplicarlos */
    transitionEase  :	"linear",

    /* Cantidad de eventos de la rueda del mouse que debe esperar el navegador antes de activar la transición */
    scrollSpeed     : {
                      'chrome' : 14,
                      'firefox': 16
                    },

    /* Puntos de navegación en la barra lateral derecha */
    navigationDots	: true,

    /* Navegación superior */
    navigationMenu  : false,

    /* Por defecto se coloca la navegación en el elemento .main, se puede cambiar la ubicación */
    navContMenu     : '',

    /* Clase de activación del menu al hacer scroll si uno quiere que tenga un cambio */
    navActiveClass	: '.opos-scroll',

    /* Altura de scrollTop del main para que se active la clase */
    navActiveScroll : 30,

    /* PushState Opcional*/
    CustomURL				: {
     'activeURL'	: false,
     'basepath' 	: '',
     'segmentURL':	''
    },

    /* Tipo de dispositivo, por defecto es desktop y el plug in se detendrá si es otro dispositivo */
    device          : 'desktop',

    /* Para para el lock scroll opos revisará la clase '.opos-disable' en el html, en caso de no tenerla funcionará regularmente, esta clase puede cambiarse o pueden ser varias */
    DisableOposWheel: '.opos-disable',

    /* Lock view es para blockear la vista actual si se detecta alguna clase en html, en este caso la de defecto es '.fancybox-lock', clase que se añade en html al abrir cualquier fancybox, puede esperar varias clases */
    LockView        : '.fancybox-lock',

    /* Callback function en caso de ser necesario, llamar algun evento al finalizar el scroll */
    onFinishScroll  : function() {},

    /* Callback function en caso de ser necesario antes de que cargue el plugin */
    beforeLoad      : function() {},

    /* Callback function en caso de ser necesario después de que cargue el plugin */
    afterLoad       : function() {}
  });

```
