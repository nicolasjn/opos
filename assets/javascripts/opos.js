/**
	@preserve
	@name: One page one scroll
	@author: Nicolás Juárez Nuño
	@version: 1.0
*/
;(function ( $, window, document, undefined ) {

	"use strict";

		var pluginName = "opos",
			defaults = {
				pageClass       : 'opos-page',
				animationDelay  : 900,
				transitionSpeed : 600,
				transitionEase	:	"linear",
				scrollSpeed     : {
				'chrome' : 14,
				'firefox': 16
				},
				navigationDots	: true,
				navigationMenu	: false,
				navContMenu			: '',
				navActiveClass	: '.opos-scroll',
				navActiveScroll	: 30,
				CustomURL				: {
					'activeURL'	: false,
					'basepath' 	: '',
					'segmentURL':	''
				},
				device          : 'desktop',
				DisableOposWheel: '.opos-disable',
				LockView				: '.fancybox-lock',
				onFinishScroll	: function() {},
				beforeLoad			: function() {},
				afterLoad				: function() {}
		};

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				this.settings = $.extend( {}, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}

		// Avoid Plugin.prototype conflicts
		$.extend(Plugin.prototype, {
        page_index      : 0,
        pages           : {},
        arr             : '',
        $window         : $(window),
        $html_body      : $('html,body'),
				init: function () {
            var _self     = this;
            var pageClass = this.settings['pageClass'];

            _self._beforeload(pageClass);
            _self._onresize(pageClass);
            _self._onload();


				},
        _beforeload: function(pageClass)
        {
          var _self = this;

					this.settings.beforeLoad.call(this);
          _self.pagesInit( pageClass, _self.$window);

        },

        _onload: function()
        {

          var _self = this;
          _self.$window.on('load', function(){

            _self.wheelNavigation();

						if( _self.settings.CustomURL['activeURL'] )
							_self.scrollToSegment();

						_self.settings.afterLoad.call(this);

          });

        },

        _onresize: function(pageClass)
        {

          var _self = this;
          _self.$window.on('resize', function(){

            //Cambiar valores del array al cambiar tamaño de ventana
						_self.pageHeight(pageClass, $(this) );
            _self.pageArr(pageClass);
          });

        },

        pagesInit: function(page_class, window_objet)
        {
          var _self = this;

          _self.pageClassinit(page_class);
          _self.pageHeight(page_class, window_objet);
          _self.pageArr(page_class);

        },

        pageHeight: function(page_class, window_objet)
        {
          $('.' + page_class).css({'min-height' : window_objet.height() });
        },

        pageClassinit: function(page_class)
        {
          $(this.element).children().addClass(page_class).wrapAll("<div class='maininner'><div class='none-scrollbar'></div></div>");
        },

        pageArr: function(pageClass)
        {
          var _self = this;
          _self.arr = $('.' + pageClass).map(function(i, el) {
            return _self.pages[$(el).attr('id')] = -1*$('.maininner').offset().top + $(this).offset().top + '-' + $(this).outerHeight() + '-' + (parseInt($(this).index()));
          }).get();

        },

        transitionClass: function(_index, direction)
        {
          var _self = this;
          if(direction == 'down') {
            $('.' + this.settings['pageClass']).eq(_index).addClass('page_transition_next').removeClass('page_transition_prev');
          } else {
            $('.' + this.settings['pageClass']).eq(_index).addClass('page_transition_prev').removeClass('page_transition_next');
          }
        },

        prevAllClasses: function(section)
        {

          $('#' + section).removeClass('page_transition_next').siblings().removeClass('page_transition_next');
          $('#' + section).prevAll().each(function(){
            var $this = $(this);
            if( !$this.hasClass('page_transition_next') ) {
              $this.addClass('page_transition_next');
            }
          });

        },


        pageNavigationDots: function()
        {
          var _self = this;
          var html  = '';

					$.each( _self.pages , function( _id , _n) {
						var elm         = _n.split('-'),
								elm_index   = parseInt(elm[2]);
					 html+= '<div class="opos-dot dot-' + elm_index + '"></div>';
					});

          $(this.element).append('<div class="opos-controls"><div class="opos-dotsnav">' + html + '</div></div>');

        },

				pageNavigation: function()
				{

					var _self	= this;
					var html	= '';

					$('.' + _self.settings['pageClass']).each(function(i){
						var $this = $(this);
						html+='<!-- --><li class="opos-nav-item"><a href="#'+ $this.attr('id') +'" class="nav-item nav-item-'+ i +'" data-page="' + $this.attr('data-name') + '"><span>' + $this.attr('data-name') + '</span></a></li><!-- -->'
					});

					if( $(this.settings['navContMenu']) != undefined && $(this.settings['navContMenu']).length == 1 ) {
						$(this.settings['navContMenu']).append('<nav class="opos-nav"><ul>' + html +'</ul></nav>');
					} else {
						$(this.element).append('<nav class="opos-nav"><ul>' + html +'</ul></nav>');
					}

				},

        pageAnimation: function(_index, delay)
        {

          var _self   = this;
          var _mainin = $('.maininner');
          var _delay  = delay == null ? 0 : this.settings['animationDelay'];
          var _speed  = this.settings['transitionSpeed'];
          var _scroll = _self.settings.device === 'desktop' ? -1*$('.none-scrollbar').offset().top + $('.' + this.settings['pageClass']).eq(_index).offset().top : $('.' + this.settings['pageClass']).eq(_index).offset().top;
          var elem    = _self.settings.device === 'desktop' ? $(this.element) : _self.$html_body;

          elem.stop().delay(_delay).animate({
              scrollTop: _scroll },
              {
              duration: _speed,
              easing: _self.settings['transitionEase'],
              complete: function(){
                if(_self.page_index != _index)
                  _self.page_index = _index;
                _mainin.trigger('finish_scroll');
              }
          });

					_self._onFinishScroll(_mainin);

        },

				_onFinishScroll: function(elem)
				{

					var _self		= this;

					elem.on('finish_scroll', function(){
						_self.settings.onFinishScroll.call(_self);
					});

				},

				ajaxPushstate: function(url, section, from_history)
				{

					var _self = this;

					var stateObj = { data: section };
					history.pushState(stateObj, '', url);

					_self.prevAllClasses(section);
					_self.pageAnimation($('#' + section).index());


				},

				scrollToSegment: function()
			  {
			    var _self = this,
			        check = _self.settings.CustomURL['segmentURL'] != undefined && _self.settings.CustomURL['segmentURL'] != false  ? _self.settings.CustomURL['segmentURL'].split("/") : false;


			    if( check == false || check.length > 1 ) {
			      _self.page_index = 0;
			    } else {
			      _self.page_index = $('#' +_self.settings.CustomURL['segmentURL']).index();
			    }

					_self.pageAnimation( _self.page_index, this.settings['animationDelay'] );
			    _self.prevAllClasses($('.' + _self.settings['pageClass']).eq(_self.page_index).attr('id'));

			  },

        wheelNavigation: function()
        {
          var _self         = this,
              delta         = 0,
              contador_up   = 0,
              contador_down = 0,
              direction     = '',
              limt_diferenc = 20,
              is_chrome     = navigator.userAgent.toLowerCase().indexOf('chrome') > -1,
              limit         = is_chrome == true ? this.settings['scrollSpeed']['chrome'] : this.settings['scrollSpeed']['firefox'],
              higher        = false,
              $pages        = $('.' + this.settings['pageClass']),
              page_limit    = $pages.length,
              scroll        = true;

          var _mainin = $('.maininner');


          // Defino una clase para saber si se activo todo
          if(_self.settings.device === 'desktop' && !$('html').is(_self.settings['DisableOposWheel'])) {

            $(this.element).addClass('opos-active');

            function wheel(event){
              delta = 0;
              if (!event) { /* For IE. */
                event = window.event;
              }
              if (event.wheelDelta) { /* IE/Opera. */
                delta = event.wheelDelta/120;
              } else if (event.detail) { /** Mozilla case. */
                delta = -event.detail/3;
              }

              if(scroll){
                if (delta) {
                  handle(delta);
                }
              }

              if( higher == false ) {
                if (event.preventDefault) {
                  event.preventDefault();
                }
                event.returnValue = false;
              }

            }

            if (window.addEventListener) {
                 /** DOMMouseScroll is for mozilla. */
                 window.addEventListener('DOMMouseScroll', wheel, false);
            }
            /** IE/Opera. */
            window.onmousewheel = document.onmousewheel = wheel;


            // Detect Down or Up
            function handle(delta) {
              if( !$('html').is(_self.settings['LockView']) ){
                if (delta < 0) {
                    direction = 'down';
                    contador_down+=1;
                    if(contador_down >= limit){
                      contador_down = 0;
                      if( !higher ) {
                        if( _self.page_index < page_limit-1 && _self.page_index != page_limit) {
                          _self.transitionClass(_self.page_index, direction);
                          _self.page_index+=1;
                          scroll = false;
                          _self.pageAnimation(_self.page_index, _self.settings['animationDelay']);
                          _mainin.on('finish_scroll', function(){
                            scroll = true;
                          });

                        }
                      }

                    }

                } else {
                    direction = 'up';
                    contador_up+=1;
                    if(contador_up >= limit){
                      contador_up = 0;

                      if( !higher ) {
                        if( _self.page_index > 0 && _self.page_index != 0) {
                          _self.page_index-=1;
                          _self.transitionClass(_self.page_index, direction);
                          _self.pageAnimation(_self.page_index);
                        }
                      }
                    }
                }

              }// IF fancybox active

            }

          }

          /*--------------------------------------------------------------------------
          | Nav Events
          --------------------------------------------------------------------------*/
          if(_self.settings.device == 'desktop') {

						if(_self.settings.navigationDots == true) {
	            _self.pageNavigationDots();

	            $(this.element).on('click', '.opos-dot', function(evt){
	              var $this = $(this);
	              $this.addClass('current-dot').siblings().removeClass('current-dot');
	              _self.pageAnimation($this.index());
	              _self.pageIndex = $this.index();
	              _self.prevAllClasses($('.' + _self.settings['pageClass']).eq(_self.page_index).attr('id'));

	            });

						}

          }

					if(_self.settings.navigationMenu == true) {

						_self.pageNavigation();

						var BasePath = _self.settings.CustomURL['basepath'] == undefined || _self.settings.CustomURL['basepath'] == '' ? '' : _self.settings.CustomURL['basepath'];

						$(this.element).on('click', '.nav-item', function(evt){
							evt.preventDefault();

							var $this = $(this);
							$this.parent('li').addClass('current').siblings().removeClass('current');
							_self.pageIndex = $this.parent('li').index();



							if( _self.settings.CustomURL['activeURL'] ) {

								var _href    = BasePath + $this.attr('href').replace('#',''),
										_section = $this.attr('href').replace('#','');
								_self.ajaxPushstate(_href, _section);

							} else {

								_self.pageAnimation($this.parent('li').index());
								_self.prevAllClasses($('.' + _self.settings['pageClass']).eq(_self.page_index).attr('id'));

							}


						});

						if(_self.settings.CustomURL['activeURL']) {

							_self.$window.on('popstate', function(event){

					        event.preventDefault();

					        if(event.originalEvent.state) {
					          var _section = event.originalEvent.state.data;
					        } else {
					          var _section = $('.' + _self.settings['pageClass']).eq(0).attr('id');
					        }

					        _self.ajaxPushstate(BasePath, _section, true);


					    });

						}// Active url

					}// Active Menu


          $(this.element).on('scroll mousewheel DOMMouseScroll', function(){

            var $this   = $(this),
                trail   = $this.scrollTop(),
                w_h     = $this.height(),
                v_trail = trail + w_h;

						if( trail > _self.settings['navActiveScroll'] ) {
							$('.opos-nav').addClass( _self.settings['navActiveClass'].replace('.', '') + '-' + _self.settings['navActiveScroll']);
						} else {
							$('.opos-nav').removeClass( _self.settings['navActiveClass'].replace('.', '') + '-' + _self.settings['navActiveScroll']);
						}

            // Itero el array creado para comparar datos al hacer scroll
            $.each( _self.pages , function( _id , _n) {
              var elm         = _n.split('-'),
                  elm_offset  = parseInt(elm[0]),
                  elm_height  = parseInt(elm[1]),
                  elm_index   = parseInt(elm[2]);

								console.log(elm_offset);

              // En vista
              if(trail >= elm_offset && v_trail <= (elm_offset + elm_height) ) {
                // Guardo elemento actual en variable
                var $elm = $('#' + _id);

                // Agrego una clase al pasar por viewport
                if( !$('#' + _id).hasClass('in-viewport') ) {
                  $elm.addClass('in-viewport').siblings().removeClass('in-viewport');
                  $('a[href="#'+ _id +'"]').parent('li').addClass('active').siblings('li').removeClass('active');

                  if( _self.settings.device == 'desktop') {
                    $('.opos-dot').eq(elm_index).addClass('current-dot').siblings().removeClass('current-dot');
                  }
                }

              } /* Termina if 'En vista' */

              if( _self.settings.device === 'desktop' ) {

                if( trail >= elm_offset - 100 && _self.page_index == elm_index ) {

                  // Guardo elemento actual en variable
                  var $elm = $('#' + _id);

                  if( elm_height > w_h ) {
                    // Reviso si es más grande que el viewport y dependiendo de esto activo/desactivo el scroll
                    higher = true;

                    // Checkear si está al tope
                    if( elm_offset == trail || trail <= elm_offset ) {
                      // Reviso si está en el tope
                      $elm.addClass('in-top').siblings().removeClass('in-top'); ;

                      // En caso de estar en el tope y hacer scroll para arriba vuelvo al funcionamiento normal
                      if( direction == 'up' && $elm.hasClass('in-top') ) {
                          higher = false;
                      } else if( direction == 'down' ) {
                          higher = true;
                      }

                    } else {

                      // En caso de estar en el tope y hacer scroll para abajo vuelvo al funcionamiento de pantalla alta
                      $elm.removeClass('in-top');
                      if( direction == 'down' && !$elm.hasClass('in-top')) {
                          higher = true;
                      }

                    }

                    if ( (elm_offset + elm_height - limt_diferenc ) == v_trail || v_trail >= (elm_offset + elm_height - limt_diferenc) ) {
                      // Reviso si estamos en la parte de abajo de la sección y armamos las mismas situaciones que arriba
                      $elm.addClass('in-bottom').siblings().removeClass('in-bottom'); ;
                      if( direction == 'down' && $elm.hasClass('in-bottom') ) {
                        higher = false;

                      } else {

                        $elm.removeClass('in-bottom');
                        if( direction == 'up' && !$elm.hasClass('in-bottom') ) {
                            higher = true;
                        }

                      }

                    } /* Termina por debajo */


                  } else {

                    // Reseteo las clases de los hermanos
                    if( $elm.siblings().hasClass('in-top') ) {
                      $elm.siblings().removeClass('in-top');
                    }
                    if($elm.siblings().hasClass('in-bottom')) {
                      $elm.siblings().removeClass('in-bottom');
                    }

                    higher = false;

                  } /* Termina if 'Al tope' */

                } /* Termina en seccion por index */

              } /* if is desktop */


            }); /* Termina each */

          }).scroll();

        },

		});

		// A really lightweight plugin wrapper around the constructor,
		// preventing against multiple instantiations
		$.fn[ pluginName ] = function ( options ) {
				return this.each(function() {
						if ( !$.data( this, "plugin_" + pluginName ) ) {
								$.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
						}
				});
		};

})( jQuery, window, document );
